#!/bin/bash

SOURCE_DEVICE="$1"
SOURCE_FOLDER="$2"
TARGET_DEVICE="$3"
TARGET_FOLDER="$4"
MOUNT_ROOT="$5"
SOURCE_PATH="$MOUNT_ROOT$SOURCE_DEVICE$SOURCE_FOLDER"
TARGET_PATH="$MOUNT_ROOT$TARGET_DEVICE$TARGET_FOLDER"
CP_COMMAND="/usr/bin/cp --verbose -rfp"
CP_PROCESS='cp'
NOHUP_COMMAND="nohup $CP_COMMAND $SOURCE_PATH $TARGET_PATH/"
NOHUP_LOG="/home/pi/backup-box/log/backup.log"

if pgrep -x "$CP_PROCESS" >/dev/null
then
    echo "Backup bereits aktiv!"
    exit 3
fi

if [ "$SOURCE_DEVICE" = "" ]
then
    echo "Kein Quelllaufwerk definiert!"
    exit 1
fi

if [ "$TARGET_DEVICE" = "" ]
then
    echo "Kein Ziellaufwerk definiert!"
    exit 2
fi

if [ ! -d "$MOUNT_ROOT$SOURCE_DEVICE" ]
then
    echo "Kein Quelllaufwerk verbunden!"
    exit 4
fi

if [ ! -d "$MOUNT_ROOT$TARGET_DEVICE" ]
then
    echo "Keine Ziellaufwerk verbunden!"
    exit 5
fi

echo "Starte Backup."
mkdir -p "$TARGET_PATH"
if [ -f "$NOHUP_LOG" ]
then
    rm "$NOHUP_LOG"
fi
$NOHUP_COMMAND > "$NOHUP_LOG" &
echo "Backup im Hintergrund aktiv."
exit 0
