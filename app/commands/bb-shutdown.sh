#!/bin/bash

CP_PROCESS='cp'
NOHUP_LOG="/home/pi/backup-box/log/backup.log"

if pgrep -x "$CP_PROCESS" >/dev/null
then
    echo "Backup derzeit aktiv!"
    exit 1
fi

if [ -f "$NOHUP_LOG" ]; then
    rm "$NOHUP_LOG"
fi

sync
umount /dev/sda1 &>/dev/null
umount /dev/sdb1 &>/dev/null
umount /dev/sdc1 &>/dev/null
/usr/sbin/shutdown -P 1
echo "Angeschlossene Geräte sicher getrennt."
echo "Raspberry fährt in Kürze herunter."
exit 0
