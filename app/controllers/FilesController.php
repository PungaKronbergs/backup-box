<?php

class FilesController extends BaseController
{
	/**
	 * Provide File Manager Main Page
	 *
	 * @param $f3
	 * @return void
	 */
	public function render($f3): void
	{
		global $CONFIG;

		define('FM_EMBED', true);
		define('FM_LANG', 'de');
		define('FM_SELF_URL', '/files');
		define('FM_ROOT_PATH', '/');
		define('FM_TRANSLATION_FILE', 'app/config/translation.json');

		foreach($_GET as $key => $value) {
			$_GET[ltrim($key, '?')] = $value;
		}

		require_once('app/helper/FilesHelper.php');
	}

}