<?php

class SettingsController extends BaseController
{
	/**
	 * Provide Settings page
	 *
	 * @param $f3
	 * @return void
	 */
	public function index($f3): void
	{
		$mountRoot = $this->config['cfg-mount-root'];
		$dateFormat = $this->config['cfg-date-format'];
		$mounts = $this->helper->getActiveMounts($mountRoot);

		$f3->set('mountRoot', $mountRoot);
		$f3->set('dateFormat', $dateFormat);
		$f3->set('mounts', $mounts);
		$f3->set('config', $this->config);

		$template = new Template;
		echo $template->render('settings.htm');
	}

	/**
	 * Settings page - process form submit
	 *
	 * @param $f3
	 * @return void
	 */
	public function processForm($f3): void
	{
		$this->helper->saveSettings();
		$f3->set('output', 'Einstellungen gespeichert!');

		$config = $this->helper->getCurrentSettings();
		$mountRoot = $config['cfg-mount-root'];
		$dateFormat = $config['cfg-date-format'];
		$mounts = $this->helper->getActiveMounts($mountRoot);
		
		$f3->set('mountRoot', $mountRoot);
		$f3->set('dateFormat', $dateFormat);
		$f3->set('mounts', $mounts);
		$f3->set('config', $config);

		$template = new Template;
		echo $template->render('settings.htm');
	}

}