#!/bin/bash

CP_PROCESS='cp'

if pgrep -x "$CP_PROCESS" >/dev/null
then
    ps -ef | grep "$CP_PROCESS" | grep -v grep | awk '{print $2}' | xargs kill
    echo "Backup abgebrochen!"
    exit 0
else
    echo "Kein Backup aktiv!"
    exit 1
fi

