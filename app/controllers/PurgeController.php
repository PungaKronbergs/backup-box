<?php

class PurgeController extends BaseController
{

	/**
	 * Provide Purge page
	 *
	 * @param $f3
	 * @return void
	 */
	public function index($f3): void
	{
		$f3->set('output', '');

		$template = new Template;
		echo $template->render('purge.htm');
	}

	/**
	 * Purge page - process form submit
	 *
	 * @param $f3
	 * @return void
	 */
	public function processForm($f3): void
	{
		$f3->set('output', $this->helper->executeCommand());

		$template = new Template;
		echo $template->render('purge.htm');
	}

}