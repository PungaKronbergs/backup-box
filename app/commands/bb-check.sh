#!/bin/bash

CP_PROCESS='cp'

if pgrep -x "$CP_PROCESS" >/dev/null
then
    echo "Backup aktiv!"
    uptime
    exit 1
else
    echo "Kein Backup aktiv!"
    exit 0
fi
