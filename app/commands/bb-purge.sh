#!/bin/bash

MOUNT_ROOT="$1"
CP_PROCESS='cp'

if pgrep -x "$CP_PROCESS" >/dev/null
then
    echo "Backup derzeit aktiv!"
    exit 1
fi

sync
umount /dev/sda1 &>/dev/null
umount /dev/sdb1 &>/dev/null
umount /dev/sdc1 &>/dev/null
umount /dev/sde1 &>/dev/null

if [ "$MOUNT_ROOT" != "/" ]
then
  rm "$MOUNT_ROOT" -r
fi
echo "Alle Mountpunkte gelöscht!"
