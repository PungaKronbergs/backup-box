# Backup Box

Mobile Toolbox zum Sichern von SD-Card Inhalten auf eine externe Festplatte via Raspberry Pi 4. 

## Funktionsweise
Auf einem Raspberry Pi 4 werden diverse Scripte installiert, mittels deren Hilfe die Daten einer angeschlossenen SD-Karte automatisiert auf eine ebenfalls angeschlossene USB-Festplatte kopiert werden können.
Gerade unterwegs sehr hilfreich, wenn man ein Backup einer SD-Card benötigt oder diese für eine neuerliche Verwendung leeren muss, ohne die bereits gesammelten Daten zu verlieren.
Nötig sind dafür nur ein Smartphone, ein Raspberry 4, eine SD-Card mit USB-Kartenleser und eine 2,5" USB-Festplatte mit möglichst geringem Stromverbrauch. 

Zur Nutzung wird auf dem Smartphone für den Zeitraum der Sicherung ein Hotspot aktiviert, mit dem sich der Raspberry dann automatisch verbindet. Einmal verbunden kann man dann über das Web-Frontend auf dem Smartphone Backups wie gewünscht durchführen.
          
**Wichtig:** Für den Raspberry wird ein Netzteil mit mind. 3A benötigt! Je nach Strombedarf von Kartenleser, Festplatte und anderem verbauten Equipment (z.B. Lüfter) kann sogar ein noch stärkeres Netzteil nötig sein.   

## Installation

Mit dem [Raspberry Pi Imager](https://www.raspberrypi.com/software/) eine SD-Card mit Bullseye 64 Bit (inkl. Desktop!) flashen. SSH Zugang mittels leerer Datei namens _ssh_ auf der SD-Card aktivieren.
Raspberry mit dieser SD-Card hochfahren und via SSH als user _pi_ anmelden (initiales Kennwort _raspberry_). 

Danach folgende Befehle durchführen:

``` 
cd ~
git clone https://gitlab.com/PungaKronbergs/backup-box.git
cd backup-box
make
```

Nun noch mittels _Raspi-Config_ nötige Einstellungen vornehmen

```
# WLAN auf Smartphone Hotspot einstellen
# Eigenes Kennwort setzen
# Hostname auf BACKUP-BOX
# Zeitzone auf Europe->Berlin

sudo raspi-config
sudo reboot
```
Bei Bedarf den eigenen SSH-Key in _~/.ssh/authorized_keys_ eintragen.

## Verwendung via Web Frontend
Hat sich der Raspberry mit dem Hotspot des Smartphones verbunden, ermittelt man die zugehörige IP des Raspberrys (wird meist im Smartphone angezeigt).
Via Browser verbindet man sich auf dem Smartphone mit dieser IP und hat dann Zugriff auf das Web-Frontend, mittels dessen man die verschiedenen Funktionen der Backup-Box komfortabel steuern kann.

## Credits

- Dateimanager [Tiny File Manager](https://github.com/prasathmani/tinyfilemanager)  
- PHP Framework [Fat-Free Micro-Framework](https://github.com/bcosca/fatfree) 
