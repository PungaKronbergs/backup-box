<?php

class BaseController
{
	protected MainHelper $helper;
	protected $config;

	function __construct()
	{
		$this->helper = new MainHelper();
		$this->config = $this->helper->getCurrentSettings();
	}

	function beforeroute()
	{
	}

	function afterroute()
	{
	}

}