<?php

class IndexController extends BaseController
{

	/**
	 * Provide Index page
	 *
	 * @param $f3
	 * @return void
	 */
	public function index($f3): void
	{
		$f3->set('output', $this->helper->getCurrentStatus());
		$f3->set('lastlog', $this->helper->getLastLogMessages());

		$template = new Template;
		echo $template->render('index.htm');
	}

	/**
	 * Index page - process form submit
	 *
	 * @param $f3
	 * @return void
	 */
	public function processForm($f3): void
	{
		$f3->set('output', $this->helper->executeCommand());
		$f3->set('lastlog', $this->helper->getLastLogMessages());

		$template = new Template;
		echo $template->render('index.htm');
	}

}