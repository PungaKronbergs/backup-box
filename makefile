init:
	sudo apt-get -y update
	sudo apt-get -y dist-upgrade
	sudo apt-get -y install lighttpd php-cgi mc php-mbstring php-zip composer
	sudo apt-get -y install libbz2-dev libpcre++-dev libpcre2-dev dpkg-dev zlib1g-dev autoconf
	sudo apt-get -y remove pulseaudio
	sudo apt -y autoremove
	composer install --no-progress --no-interaction --optimize-autoloader
	sudo lighttpd-enable-mod fastcgi
	sudo lighttpd-enable-mod fastcgi-php
	sudo lighttpd-enable-mod rewrite
	sudo systemctl stop lighttpd.service
	sudo mv /usr/sbin/lighttpd /usr/sbin/lighttpd.orig
	sudo mv /usr/sbin/lighttpd-angel /usr/sbin/lighttpd-angel.orig
	mkdir /home/pi/lighttpd
	git clone https://salsa.debian.org/debian/lighttpd.git /home/pi/lighttpd/
	sed -i 's!(pwd->pw_uid == 0)!(pwd->pw_uid == 99999999)!' /home/pi/lighttpd/src/server.c
	sed -i 's!(grp->gr_gid == 0)!(grp->gr_gid == 99999999)!' /home/pi/lighttpd/src/server.c
	cd /home/pi/lighttpd && ./configure && make && sudo make install
	sudo cp -f /home/pi/backup-box/assets/lighttpd.conf /etc/lighttpd/lighttpd.conf
	sudo cp -f /home/pi/backup-box/assets/lighttpd /etc/init.d/lighttpd
	sudo cp -f /home/pi/backup-box/assets/motd /etc/motd
	sudo mv /usr/local/sbin/lighttpd* /usr/sbin
	sudo chmod 755 /etc/init.d/lighttpd
	sudo chmod +x /home/pi/backup-box/app/commands/*
	sudo systemctl start lighttpd.service
	sudo rm /home/pi/lighttpd -r
