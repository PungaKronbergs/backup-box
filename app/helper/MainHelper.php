<?php

class MainHelper
{
	const MOUNT_ROOT = '/media/pi/';
	const DATE_FORMAT = 'Y-m-d-H-i';
	const CONFIG_FILE = '/home/pi/backup-box/app/config/settings.json';
	const LOG_FILE = '/home/pi/backup-box/log/backup.log';
	const CMD_BACKUP = '/home/pi/backup-box/app/commands/bb-start.sh "%s" "%s" "%s" "%s" "%s"';
	const CMD_STOP = '/home/pi/backup-box/app/commands/bb-stop.sh';
	const CMD_SHUTDOWN = '/home/pi/backup-box/app/commands/bb-shutdown.sh';
	const CMD_PURGE = '/home/pi/backup-box/app/commands/bb-purge.sh "%s"';
	const CMD_CHECK = '/home/pi/backup-box/app/commands/bb-check.sh';

	/**
	 * Return all currently mounted devices
	 *
	 * @return array
	 */
	public function getActiveMounts(string $mountRoot): array
	{
		$result = [];
		$dirInfo = @scandir($mountRoot);
		if (!empty($dirInfo)) {
			foreach ($dirInfo as $key => $value) {
				if (!in_array($value, array(".", "..")) && is_dir($mountRoot.$value)) {
					$result[] = $value;
				}
			}
		}

		return $result;
	}

	/**
	 * Save user settings
	 *
	 * @return void
	 */
	public function saveSettings(): void
	{
		$config = $this->getCurrentSettings();

		if (!empty($_POST['device'])) {
			foreach ($_POST['device'] as $device) {
				if (in_array($device['type'], ['T', 'S']) && !empty($device['id'])) {
					$device['path'] = $this->addDirectorySeperator(true, false, $device['path']);
					if ($device['type'] == 'T') {
						$device['path'] = $this->addDirectorySeperator(false, true, $device['path']);
					}
					$config[$device['id']] = $device;
				}
			}
		}

		$config['cfg-mount-root'] = $_POST['mount_root'] ? $this->addDirectorySeperator(
		   true,
		   true,
		   $_POST['mount_root']
		) : self::MOUNT_ROOT;

		if (empty($config['cfg-mount-root']) || $config['cfg-mount-root'] == '/') {
			$config['cfg-mount-root'] = self::MOUNT_ROOT;
		}

		$config['cfg-date-format'] = $_POST['date_format'] ? $this->sanitizeFolderName(
		   $_POST['date_format']
		) : self::DATE_FORMAT;

		if (empty($config['cfg-date-format']) || $config['cfg-date-format'] == '/') {
			$config['cfg-date-format'] = self::DATE_FORMAT;
		}

		file_put_contents(self::CONFIG_FILE, json_encode($config, JSON_PRETTY_PRINT));
	}

	/**
	 * Return current configuration
	 *
	 * @return array
	 */
	public function getCurrentSettings(): array
	{
		$cfg = [];

		if (is_file(self::CONFIG_FILE) && is_readable(self::CONFIG_FILE)) {
			$cfg = json_decode(file_get_contents(self::CONFIG_FILE), true);
		}

		if (empty($cfg['cfg-mount-root']) || $cfg['cfg-mount-root'] == '/') {
			$cfg['cfg-mount-root'] = self::MOUNT_ROOT;
		}
		if (empty($cfg['cfg-date-format']) || $cfg['cfg-date-format'] == '/') {
			$cfg['cfg-date-format'] = self::DATE_FORMAT;
		}

		return $cfg;
	}

	/**
	 * Start selected command
	 *
	 * @return string
	 */
	public function executeCommand(): ?string
	{
		$config = $this->getCurrentSettings();
		$mountRoot = $config['cfg-mount-root'];
		$dateFormat = $config['cfg-date-format'];

		if ($_POST['start']) {
			$source = $this->getActiveDeviceForType('S');
			$target = $this->getActiveDeviceForType('T');
			if (empty($source)) {
				return 'Kein Quelllaufwerk definiert!';
			}
			if (empty($target)) {
				return 'Kein Ziellaufwerk definiert!';
			}

			return shell_exec(
			   sprintf(
				  self::CMD_BACKUP,
				  $source['id'],
				  $source['path'],
				  $target['id'],
				  $this->prepareTargetPath($target['path'], $dateFormat),
				  $mountRoot
			   )
			);
		}
		if ($_POST['kill']) {
			return shell_exec(self::CMD_STOP);
		}
		if ($_POST['shutdown']) {
			return shell_exec(self::CMD_SHUTDOWN);
		}
		if ($_POST['purge']) {
			return shell_exec(sprintf(self::CMD_PURGE, $mountRoot));
		}

		return '';
	}

	/**
	 * Get current status from check script
	 *
	 * @return string
	 */
	public function getCurrentStatus(): ?string
	{
		if (file_exists(self::CMD_CHECK)) {
			return shell_exec(self::CMD_CHECK);
		}

		return '';
	}

	/**
	 * Get log content
	 *
	 * @return string
	 */
	public function getLastLogMessages(): string
	{
		if (file_exists(self::LOG_FILE)) {
			return file_get_contents(self::LOG_FILE);
		}

		return '';
	}

	/**
	 * Get active device for given type
	 *
	 * @param string $type
	 * @return string|null
	 */
	public function getActiveDeviceForType(string $type): array
	{
		$config = $this->getCurrentSettings();
		$mounts = $this->getActiveMounts($config['cfg-mount-root']);

		foreach ($mounts as $mount) {
			if (array_key_exists($mount, $config) && $config[$mount]['type'] == $type) {
				return $config[$mount];
			}
		}

		return [];
	}

	/**
	 * Return full target path
	 *
	 * @param string $path
	 * @param string $dateFormat
	 * @return string
	 */
	public function prepareTargetPath(string $path, string $dateFormat): string
	{
		return $path.date($dateFormat);
	}

	/**
	 * Sanitize given folder name
	 *
	 * @param string $name
	 * @return string
	 */
	public function sanitizeFolderName(string $name): string
	{
		return preg_replace('/[^a-zA-Z0-9-\._]+/', '-', $name);
	}

	/**
	 * Return string with slash at given positions
	 *
	 * @param bool $prefix
	 * @param bool $suffix
	 * @param string $value
	 * @return string
	 */
	public function addDirectorySeperator(bool $prefix, bool $suffix, string $value): string
	{
		if ($prefix) {
			$value = '/'.ltrim($value, '/');
		}
		if ($suffix) {
			$value = rtrim($value, '/').'/';
		}

		return $value;
	}

}
